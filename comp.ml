(** Module of performing translation from typed AST to quad IR. *)

open Ast
open Common
open List
open Printf
open Quad

(** Position of variables in memory. *)
type var =
  | NOLOC
  | LOCAL of int
  | GLOBAL of string


(** Empty environment. *)
let empty_env = fun _ -> NOLOC

(** Add a value to the environment. *)
let add_env e i l = fun i' -> if i = i' then l else e i'


(** Get value of an environment. *)
let get_env e i = e i


(** Return alignment constraint of the given type.
    Target dependent function (ARM implementation).
    @param t	Type.
    @return		Alignment constraint. *)
let align t =
  match t with
  | VOID
  | FUN _	-> failwith "Comp.align"
  | INT
  | PTR _
  | FLOAT	-> 4
  | CHAR	-> 1


(** Get the size in bytes of the given type.
    Target dependent function (ARM implementation).
    @param t	Type to get size for.
    @return		Type size in bytes. *)
let sizeof t =
  match t with
  | VOID
  | FUN _	-> failwith "Comp.align"
  | INT
  | PTR _
  | FLOAT	-> 4
  | CHAR	-> 1


(** Get the size in bytes of the given type to store it in the stack
    as a parameter.
    Target dependent function (ARM implementation).
    @param t	Type to get size for.
    @return		Type size in bytes. *)
let param_size t =
  match t with
  | VOID
  | FUN _	-> failwith "Comp.align"
  | INT
  | PTR _
  | FLOAT
  | CHAR	-> 4


(** Build a data entry.
    @param t	Data type.
    @param i	Data identifier.
    @param e	Data initial value.
    @return		Built data entry. *)
let make_data t i e =
  match t, e with
  | CHAR, NONE				-> QCHAR (i, 0)
  | CHAR, CST (_, INTV k)		-> QCHAR (i, k)
  | INT, NONE					-> QINT (i, 0)
  | INT, CST (_, INTV k)		-> QINT (i, k)
  | FLOAT, NONE				-> QFLOAT (i, 0.)
  | FLOAT, CST (_, FLOATV x)	-> QFLOAT (i, x)
  | PTR _, NONE				-> QINT (i, 0)
  | _							-> failwith "Comp.make_data"



(** Compile the given reference into quads.
    @param r	Reference to translate.
    @param env	Current environment.
    @return		(result register, generated quads). *)
let rec t_refr r env =
  match r with

  | RLINE (_, _, r) ->
    t_refr r env

  | ID (t, i) ->
    (match env i with
     | LOCAL k ->
       let v = new_tmp INT in
       let v' = new_tmp (PTR t) in
       (v', [QSETI (v, k); QADD (PTR t, v', fp, v)])
     | GLOBAL l ->
       let v = new_tmp (PTR t) in
       (v, [QSETL (v, l)])
     | _ ->
       failwith "Comp.r_ref ID")

  | AT (t, e) ->
    t_expr e env

  | _ ->
    failwith "Comp.t_r"


(** Compile the given expression into quads.
    @param e	Expression to translate.
    @param env	Current environment.
    @return		(result register, generated quads). *)
and t_expr e env =
  match e with
  | ELINE (_, _, e) ->t_expr e env
  | CST(t,e1) -> (
      let tmp = new_tmp t in 
      match t,e1 with 
      | INT,INTV(v) -> (tmp, [QSETI(tmp,v)])
      | FLOAT,FLOATV(v) -> (tmp, [QSETF(tmp,v)])
      | _ -> failwith("Comp.t_e.cst")
    ) 
  | REF(t,re) ->  
    let (r,q) = t_refr re env in
    let rr = new_tmp t in 
    (rr,q @ [LOAD(t,rr,r)]) 
  | UNOP(t,o,ex) ->
    let (r,q) = t_expr ex env in
    let tmp = new_tmp t in
    let qn = match o with 
      | NEG -> QNEG(t,tmp,r)
      | INV -> QINV(t,tmp,r)
      | _ -> failwith "Comp.t_e.unop"
    in
    (tmp,q @ [qn])
  | BINOP(t,o,l,r) -> (
      let ret = match o with 
        | ADD | SUB | MUL | DIV 
        | MOD | BIT_AND | BIT_OR | XOR 
        | SHL | SHR -> (
            let (lr,q1) = t_expr l env in
            let (rr,q2) = t_expr r env in
            let tmp = new_tmp t in
            let qn = match o with 
              | ADD -> QADD(t,tmp,lr,rr)
              | SUB -> QSUB(t,tmp,lr,rr)
              | MUL -> QMUL(t,tmp,lr,rr)
              | DIV -> QDIV(t,tmp,lr,rr)
              | MOD -> QMOD(t,tmp,lr,rr)
              | BIT_AND -> QAND(t,tmp,lr,rr)
              | BIT_OR -> QOR(t,tmp,lr,rr)
              | XOR -> QXOR(t,tmp,lr,rr)
              | SHL -> QSHL(t,tmp,lr,rr)
              | SHR -> QSHR(t,tmp,lr,rr)
              | _ -> failwith "Comp.t_e.binop.1"
            in
            (tmp,q1 @ q2 @ [qn])
          )
        | _ -> failwith "Comp.t_e.binop.2"
      in 
      ret 
    )
  | CAST(t,ex) ->
    let t_sub_expr = (
      match ex with 
      | BINOP(sub_t,_,_,_) | UNOP(sub_t,_,_) | CST(sub_t,_) 
      | REF(sub_t,_) | ADDR(sub_t,_) | ELINE(sub_t,_,_) 
      | CAST(sub_t,_) | ECALL(sub_t,_,_) -> sub_t
      | NONE -> failwith("Comp.t_e.cast")
    ) in
    let (r,q) = t_expr ex env in
    let tmp = new_tmp t in
    (tmp,q @ [QCAST(t,tmp,t_sub_expr,r)])
  | ECALL(t,REF(t1,e),p) -> (
      let tmp_return_reg = new_tmp t in 
      let rf,qf = t_refr e env in 
      let pl = map (fun elem -> t_expr elem env) p in
      let (ll,rl) = List.split pl in 
      let quad_p = List.concat rl in
      (
        tmp_return_reg,
        quad_p @ qf @ [CALL(t,tmp_return_reg,rf,ll)]
      )
    )
  | ADDR(r,e1) -> t_refr e1 env 
  | _ ->
    failwith "Comp.t_e"


(** Compile the given expression into the quads of a condition.
    @param r	Condition expression to translate.
    @param l_t	Label to call if the condition is true.
    @param l_f	Label to call if the condition is false.
    @param env	Current environment.
    @return		Generated quads. *)
and t_cond e l_t l_f env =

  let t_cmp f t e1 e2 =
    let v1, q1 = t_expr e1 env in
    let v2, q2 = t_expr e2 env in
    q1 @ q2 @ [f t v1 v2 l_t; GOTO l_f] in

  match e with
  (* !!TODO!! *)
  | ELINE (_, _, e)	->
    t_cond e l_t l_f env
  | BINOP(ty,LOG_OR,le,re) -> (
      let l = new_lab 0 in 
      (t_cond le l_t l env) @
      [LABEL(l)] @
      (t_cond re l_t l_f env)
    )
  | BINOP(ty,LOG_AND,le,re) -> (
      let l = new_lab 0 in 
      (t_cond le l l_f env) @
      [LABEL(l)] @
      (t_cond re l_t l_f env)
    )
  | BINOP(ty,o,l,r) -> (
      match o with
      | EQ | NE | LT | LE | GT | GE -> (
          let test a b c d =  match o with 
            | EQ ->  IFEQ(a,b,c,d) 
            | NE ->  IFNE(a,b,c,d) 
            | LT ->  IFLT(a,b,c,d) 
            | LE ->  IFLE(a,b,c,d) 
            | GT ->  IFGT(a,b,c,d) 
            | GE ->  IFGE(a,b,c,d) 
            | _ -> failwith "Comp.t_c.binop.logical-operator"
          in 
          t_cmp test ty l r 
        )
      | _ -> (
          let tmp_expr = CAST(INT,e) in 
          let re,qe = t_expr tmp_expr env in 
          let zer = new_tmp INT in 
          qe @ [QSETI(zer,0)] @ [IFNE(INT,zer,re,l_t)] @ [GOTO(l_f)]
        )
    )
  | CST(t,_) -> ( 
      let tmp_expr = CAST(INT,e) in 
      let re,qe = t_expr tmp_expr env in 
      let zer = new_tmp INT in 
      qe @ [QSETI(zer,0)] @ [IFNE(INT,zer,re,l_t)] @ [GOTO(l_f)]
    )
  | UNOP(ty,NOT,ex) -> (
      t_cond ex l_f l_t env
    )
  | _ -> failwith "Comp.t_c"
(** Compile the given statement into quads.
    @param s	Statement to translate.
    @param env	Current environment.
    @return		Generated quads. *)
let rec t_stmt s env =
  match s with
  | NOP ->[]
  | BLOCK s
  | DECLARE (_, _, s)
  | SLINE (_, s) -> t_stmt s env
  | SEQ(s1,s2) -> (t_stmt s1 env)@(t_stmt s2 env)
  | SET(t,r,v) -> ( 
      let (va,qa) = t_refr r env in
      let (ve,qe) = t_expr v env in 
      qe @ qa @ [STORE(t,va,ve)]
    )
  | IF(e,s1,s2) -> (
      let lt = new_lab 0 in 
      let lf = new_lab 0 in 
      let le = new_lab 0 in
      (t_cond e lt lf env) @ 
      [LABEL(lt)] @ 
      (t_stmt s1 env) @
      [GOTO(le)] @
      [LABEL(lf)] @
      (t_stmt s2 env) @
      [LABEL(le)] 
    )
  | WHILE(ex,st) -> (
      let lbegin = new_lab 0 in 
      let lbody = new_lab 0 in 
      let lend = new_lab 0 in 
      [LABEL(lbegin)] @
      (t_cond ex lbody lend env) @
      [LABEL(lbody)] @
      (t_stmt st env) @
      [GOTO(lbegin)] @
      [LABEL(lend)] 
    )
  | DOWHILE(st,ex) -> (
      let lbegin = new_lab 0 in 
      let lend = new_lab 0 in 
      [LABEL(lbegin)] @
      (t_stmt st env) @
      (t_cond ex lbegin lend env) @
      [LABEL(lend)] 
    )
  | CALL(REF(t,e),p) -> (
      let rf,qf = t_refr e env in 
      let pl = map (fun elem -> t_expr elem env) p in
      let (ll,rl) = List.split pl in 
      let quad_p = List.concat rl in
      (
        quad_p @ qf @ [CALL(VOID,zr,rf,ll)]
      )
    )
  | RETURN(ty,exp) -> (
      let rlab = match get_env env "$return" with | GLOBAL(l) -> l | _ -> failwith "bad label" in 
      let r,q = t_expr exp env in 
      q @ (if ty != VOID then [QSET(ty,vr,r)] else [] ) @[GOTO rlab]
    )
  | _ -> 
    failwith "Comp.t_s"


(** Compile the given declaration into a compilation unit.
    @param ds	Declaration.
    @return		Compilation unit (code, data). *)
let t_d ds =

  let rec call_expr e =
    match e with
    | NONE
    | CST _
      -> false
    | REF (_, r)
    | ADDR (_, r)
      -> call_refr r
    | UNOP (_, _, e)
    | ELINE (_, _, e)
    | CAST (_, e)
      -> call_expr e
    | BINOP (_, _, e1, e2)
      -> (call_expr e1) || (call_expr e2)
    | ECALL _
      -> true

  and call_refr r =
    match r with
    | NOREF
    | ID _
      -> false
    | AT (_, e)
      -> call_expr e
    | RLINE (_, _, r)
      -> call_refr r

  and call_stmt s =
    match s with
    | NOP
    | CASE _
    | DEFAULT
      -> false
    | DECLARE (_, _, s)
    | SLINE (_, s)
    | BLOCK s
      -> call_stmt s
    | SEQ (s1, s2)
      -> (call_stmt s1) || (call_stmt s2)
    | IF (e, s1, s2)
      -> (call_expr e) || (call_stmt s1) || (call_stmt s2)
    | WHILE (e, s)
    | DOWHILE (s, e)
    | SWITCH(e, s)
      -> (call_expr e) || (call_stmt s)
    | SET (_, r, e)
      ->  (call_refr r) || (call_expr e)
    | RETURN (_, e)
      -> call_expr e
    | CALL (e, es)
      -> true in

  let param (env, off, offs) (t, i) =
    (add_env env i (LOCAL off), off + param_size t, (i, off)::offs) in

  let local (env, off, offs) t i =
    let off = (off - (sizeof t)) land (lnot ((align t) - 1)) in
    (add_env env i (LOCAL off), off, (i, off)::offs) in

  let rec decl s (env, off, offs) =
    match s with
    | DECLARE (t, i, s) -> decl s (local (env, off, offs) t i)
    | SEQ (s1, s2)		
    | IF (_, s1, s2)	-> decl s2 (decl s1 (env, off, offs))
    | WHILE (_, s)		
    | DOWHILE (s, _)	
    | SWITCH (_, s)		
    | SLINE (_, s)
    | BLOCK s			-> decl s (env, off, offs)
    | _					-> (env, off, offs) in

  let comp (code, data, env) d =
    match d with
    | NODECL ->
      (code, data, env)
    | VARDECL (_, t, i, e) ->
      (code, data @ [make_data t i e], add_env env i (GLOBAL i))
    | FUNDECL (_, FUN (rt, ps), i, s) ->
      let do_call = call_stmt s in
      let rlab = new_lab () in
      let env = add_env env i (GLOBAL i) in
      let env', _, offs = fold_left param (env, (if do_call then 8 else 4), []) ps in
      let env', fsize, offs = decl s (env', 0, []) in
      let q =
        [ LABEL i ]
        @ (if do_call then [QPUSH lr] else [])
        @ [
          QPUSH fp;
          QSET (INT, fp, sp);
          QSETI (zr, -fsize);
          QSUB (INT, sp, sp, zr)
        ]
        @ (t_stmt s (add_env env' "$return" (GLOBAL rlab)))
        @ [
          LABEL rlab;
          QSETI (zr, -fsize);
          QADD (INT, sp, sp, zr);
          QPOP fp;
        ]
        @ [if do_call then QPOP lr else RETURN] in
      (code @ [(i, fsize, q)], data, env)
    | FUNDECL _ ->
      failwith "Comp.decs"
  in

  let (code, data, _) = fold_left comp ([], [], empty_env) ds in
  (code, data)

