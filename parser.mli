type token =
  | ID of (string)
  | INT of (int)
  | TYPE of (Common.type_t)
  | FLOAT of (float)
  | EQ
  | EQ_EQ
  | EXCLAM_EQ
  | LT
  | LT_EQ
  | GT
  | GT_EQ
  | PLUS
  | MINUS
  | STAR
  | SLASH
  | PERCENT
  | LT_LT
  | GT_GT
  | LPAR
  | RPAR
  | LBRACE
  | RBRACE
  | AND_AND
  | PIPE_PIPE
  | EXCLAM
  | AND
  | PIPE
  | CIRC
  | COM
  | SEMI
  | COL
  | EOF
  | CASE
  | DEFAULT
  | DO
  | ELSE
  | FOR
  | IF
  | RETURN
  | SWITCH
  | WHILE
  | PLUS_PLUS
  | MINUS_MINUS
  | TILDE

val prog :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
