(*	
 *	This file is part of SCC.
 *
 *	SCC is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	SCC is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with SCC.  If not, see <http://www.gnu.org/licenses/>.
 *)

(** Module dedicated to the selection of instructions. *)

open Common
open List
open Printf
open Quad

(** Registers used by an instruction. *)
type argument =
  | RREG of int		(** Read-register. *)
  | WREG of int		(** Write-register. *)

(** Instruction made of its opcode and its used registers.
    Each $r in opcode is replaced by the corresponding register. *)
type inst = string * argument list


(** Program representation as a list of instructions. *)
type prog = inst list


(* Generation of instructions. *)
let op_rrr code ri rj rk = (sprintf "\t%s $r, $r, $r" code, [WREG ri; RREG rj; RREG rk])
let add ri rj rk = op_rrr "add" ri rj rk
let sub ri rj rk = op_rrr "sub" ri rj rk
let andr ri rj rk = op_rrr "and" ri rj rk
let orr ri rj rk = op_rrr "orr" ri rj rk
let oer ri rj rk = op_rrr "oer" ri rj rk
let rsb ri rj rk = op_rrr "rsb" ri rj rk
let mul ri rj rk = op_rrr "mul" ri rj rk
let sdiv ri rj rk = op_rrr "sdiv" ri rj rk
let cmp ri rj = (sprintf "\tcmp $r, $r" , [WREG ri; RREG rj])
let mov ri rj = (sprintf "\tmov $r, $r" , [WREG ri; RREG rj])
let neg ri rj = (sprintf "\tneg $r, $r" , [WREG ri; RREG rj])
let mov_lsl ri rj rk = (sprintf "\tmov $r, $r, lsl $r" , [WREG ri; RREG rj; RREG rk])
let mov_asr ri rj rk = (sprintf "\tmov $r, $r, asr $r" , [WREG ri; RREG rj; RREG rk])
let mov_not ri rj = (sprintf "\tneg $r, $r" , [WREG ri, RREG rj])
(* Ajoutez ici les instructions manquantes. *)

let op_rri code ri rj k = (sprintf "\t%s $r, $r, #%d" code k, [WREG ri; RREG rj])
let addi ri rj k = op_rri "add" ri rj k
let subi ri rj k = op_rri "sub" ri rj k
let andi ri rj k = op_rri "and" ri rj k
let orri ri rj k = op_rri "orr" ri rj k
let eori ri rj k = op_rri "oer" ri rj k
let rsbi ri rj k = op_rri "rsb" ri rj k
let cmpi ri k = (sprintf "\tcmp $r, #%d" k , [WREG ri])
let movi_int ri k = (sprintf "\tmov $r, #%d" k, [WREG ri])
let movi_float ri k = (sprintf "\tmov $r, #%f" k, [WREG ri])
let movi_lab ri k = (sprintf "\tmov $r, #%s" k, [WREG ri])
let mov_lsli ri rj k = (sprintf "\tmov $r, $r, lsl #%d" k , [WREG ri;RREG rj])
let mov_asri ri rj k = (sprintf "\tmov $r, $r, asr #%d" k , [WREG ri;RREG rj])
let ldr_dec ri rj k = (sprintf "\tldr $r, [$r, #%d]" k , [WREG ri; RREG rj])
let str_dec ri rj k = (sprintf "\tstr $r, [$r, #%d]" k , [WREG ri; RREG rj])
let ldr ri rj = (sprintf "\tldr $r, [$r]", [WREG ri; RREG rj])
let str ri rj = (sprintf "\tstr $r, [$r]", [WREG ri; RREG rj])
(* Ajoutez ici les instructions manquantes. *)


let push vi = ("\tstr $r, [sp, #-4]!", [RREG vi])
let pop vi = ("\tldr $r, [sp], #4", [WREG vi])
let stmfd_sp rs =
  (sprintf "\tstmfd sp!, {%s}" (fold_left (fun s r -> if s = "" then "$r" else s ^ ", $r") "" rs),
   map (fun r -> RREG r) rs)
let ldmfd_sp rs =
  (sprintf "\tldmfd sp!, {%s}" (fold_left (fun s r -> if s = "" then "$r" else s ^ ", $r") "" rs),
   map (fun r -> WREG r) rs)


let b lab = (sprintf "\tb %s" lab, [])
let b_cnd cnd lab = (sprintf "\tb%s %s" cnd lab, [])
let b_eq lab = b_cnd "eq" lab
let b_ne lab = b_cnd "ne" lab
let b_gt lab = b_cnd "gt" lab
let b_ge lab = b_cnd "ge" lab
let b_lt lab = b_cnd "lt" lab
let b_le lab = b_cnd "le" lab
let b_l lab = b_cnd "l" lab
(* Ajoutez ici les instructions manquantes. *)


(** Translate a data item into instructions.
    @param d	Data item to translate.
    @return		Corresponding instructions. *)
let data_item d =
  match d with
  | QCHAR (l, k)	-> [(sprintf "%s: .byte %d" l k, [])]
  | QINT (l, k)	-> [(sprintf "%s: .int %d" l k, [])]
  | QFLOAT (l, x)	-> [(sprintf "%s: .float %f" l x, [])]


(** Translate data into instructions.
    @param ds	Data to translate.
    @return		Instructions result of data translation. *)
let rec data ds =
  match ds with
  | []	-> []
  | d::ds -> (data_item d) @ (data ds)


(** Translate the given quad into quadruplets.
    @param q	Quadruplet to translate.
    @return		Corresponding instructions. *)
let quad q =
  match q with
  | QNONE 	-> []
  | LABEL l	-> [(sprintf "%s:" l, [])]
  | RETURN	-> [("\tmov pc, lr", [])]
  | QPUSH vi	-> [push vi]
  | QPOP vi	-> [pop vi]
  | QSETI(r,v) -> [movi_int r v]
  | QSETF(r,v) -> [movi_float r v]
  | QSETL(r,v) -> [movi_lab r v]
  | QSET(t,r1,r2) -> [mov r1 r2]
  | QNEG(t,r1,r2) -> [neg r1 r2]
  | QADD(t,r1,r2,r3) -> [add r1 r2 r3]
  | QSUB(t,r1,r2,r3) -> [sub r1 r2 r3]
  | QMUL(t,r1,r2,r3) -> [mul r1 r2 r3]
  | QDIV(t,r1,r2,r3) -> [sdiv r1 r2 r3]
  | QSHL(t,r1,r2,r3) -> [mov_lsl r1 r2 r3]
  | QSHR(t,r1,r2,r3) -> [mov_asr r1 r2 r3]
  | QAND(t,r1,r2,r3) -> [andr r1 r2 r3]
  | QOR(t,r1,r2,r3) -> [orr r1 r2 r3]
  | QXOR(t,r1,r2,r3) -> [oer r1 r2 r3]
  | IFEQ(t,r1,r2,l) -> [cmp r1 r2] @ [b_eq l]
  | IFNE(t,r1,r2,l) -> [cmp r1 r2] @ [b_ne l]
  | IFLT(t,r1,r2,l) -> [cmp r1 r2] @ [b_lt l]
  | IFLE(t,r1,r2,l) -> [cmp r1 r2] @ [b_le l]
  | IFGT(t,r1,r2,l) -> [cmp r1 r2] @ [b_gt l]
  | IFGE(t,r1,r2,l) -> [cmp r1 r2] @ [b_ge l]
  | GOTO(l) -> [b l]
  | LOAD(t,r1,r2) -> [ldr r1 r2 ]
  | STORE(t,r1,r2) -> [str r2 r1 ]
  | CALL(t,r1,r2,rl) -> [("\tadd lr, pc, #4",[])] @ [("\tmov pc, $r", [RREG r2])]
  | _			-> []


(** Aggregate multiple pushes in one stm.
    @param qs	Quads to process.
    @param rs	Aggregated registers.
    @return		Instructions. *)
let rec make_stm qs rs =
  match qs with
  | (QPUSH r)::qs
    -> make_stm qs (r::rs)
  | _
    -> (stmfd_sp rs)::(quads qs)

(** Aggregate multiple pops in one stm.
    @param qs	Quads to process.
    @param rs	Aggregated registers.
    @return		Instructions. *)
and make_ldm qs rs =
  match qs with
  | (QPOP r)::qs
    -> make_ldm qs ((if r = lr then pc else r)::rs)
  | _
    -> (ldmfd_sp rs)::(quads qs)

(** Translate quads into instructions.
    @param qs	Quads to translate.
    @return		Instructions result of quads translation. *)
and quads qs =
  match qs with
  | [] -> 
    []
  | (QSETI(r1,i) as q1)::(QMUL(t,r2,r3,r4) as q2)::qs ->
    let poi = pow_of_2(i) in
    let res = if r1 == r4 && i == 0 then [movi_int r2 0]else  if r1 == r4 && i == 1 then [mov r2 r3] else if r1 == r4 && poi != -1 then [mov_lsli r2 r3 poi] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QDIV(t,r2,r3,r4) as q2)::qs ->
    let poi = pow_of_2(i) in
    let res = if r1 == r4 && i == 1 then [mov r2 r3] else if r1 == r4 && poi != -1 then [mov_asri r2 r3 poi] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (IFEQ(t,v1,v2,l1))::(GOTO(l2))::(LABEL(l3))::qs ->
    ([cmp v1 v2] @ [b_ne l2]) @ (quads qs)
  | (GOTO(l1) as q1)::(LABEL(l2) as q2)::qs ->
    let res = if l1 == l2 then [] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QADD(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 && i == 0 then [mov r2 r3]  else if r1 == r4 then [addi r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QSUB(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 && i == 0 then [mov r2 r3]  else if r1 == r4 then  [subi r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QAND(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 && i == 0 then [movi_int r2 0] else if r1 == r4 then [andi r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QOR(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 && i == 0 then [mov r2 r3] else if r1 == r4 then [orri r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QXOR(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 then [eori r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QSHL(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 then [mov_lsli r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QSETI(r1,i) as q1)::(QSHR(t,r2,r3,r4) as q2)::qs ->
    let res = if r1 == r4 then [mov_asri r2 r3 i] else (quad q1) @ (quad q2) in
    res @ (quads qs)
  | (QPUSH v)::qs ->
    make_stm qs [v]
  | (QPOP v)::qs ->
    make_ldm qs [v]
  | q::qs	->
    (quad q) @ (quads qs)


(** Perform selection of instruction in the corresponding unit.
    @param unit		Unit to work on.
    @return			Corresponding list of instructions. *)
let prog unit =
  let (fs, ds) = unit in
  (
    map (fun (i, _, qs) -> (i, quads qs)) fs,
    data ds
  )


(** Output the given instruction (replacing $r by corresponding registers).
    @param out		Output channel to use.
    @param i		Instruction to output. *)
let output_inst out i =
  let (fmt, rs) = i in

  let rname n =
    match n with
    | 11 -> "tmp"
    | 15 -> "pc"
    | 13 -> "sp"
    | 12 -> "fp"
    | 14 -> "lr"
    | _	 -> sprintf "r%d" n in

  let rec rep i rs =
    if i >= (String.length fmt) then () else
      match String.get fmt i with
      | '$'	-> esc (i + 1) rs
      | c		-> output_char out c; rep (i + 1) rs

  and esc i rs =
    match (String.get fmt i, rs) with
    | ('r', (WREG n)::rs)
    | ('r', (RREG n)::rs) ->
      (output_string out (rname n); rep (i + 1) rs)
    | _ ->
      failwith "Select.output_instr: unknown escape" in

  rep 0 rs; output_char out '\n'


(** Output the given list of instructions.
    @param out		Output channel.
    @param insts	Instructions to output. *)
let output_insts out insts =
  iter (output_inst out) insts

(** Output the given program to the given channel.
    @param out	Output channel.
    @param prog	Program to output. *)
let output_prog out prog =
  let (fs, ds) = prog in
  output_inst out ("\t.text", []);
  iter (fun (_, _, qs) -> iter (output_inst out) qs) fs;
  output_inst out ("\n\t.data", []);
  iter (output_inst out) ds


(** Select machine instructions in the given CFG.
    @param g	Quad CFG to select instructions in.
    @return		Instruction CFG after selection. *)
let cfg g =
  let (i, l, g) = g in
  (i, l, Cfg.replace (fun i v -> quads v) g)
